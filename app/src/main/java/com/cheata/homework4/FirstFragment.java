package com.cheata.homework4;


import android.content.res.Configuration;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.Objects;


/**
 * A simple {@link Fragment} subclass.
 */
public class FirstFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_first, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
       Button button = view.findViewById(R.id.btn_click);
       if(getResources().getConfiguration().orientation== Configuration.ORIENTATION_PORTRAIT) {
           button.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   assert getFragmentManager() != null;
                   getFragmentManager().beginTransaction()
                           .replace(R.id.blank_space, new SecondFragment())
                           .addToBackStack(null)
                           .commit();
               }
           });
       }
//       when your screen is landscape
        if(getResources().getConfiguration().orientation== Configuration.ORIENTATION_LANDSCAPE)
        {
            button.setVisibility(View.GONE);
        }


    }
}
